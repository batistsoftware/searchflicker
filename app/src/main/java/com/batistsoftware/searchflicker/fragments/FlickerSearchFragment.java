package com.batistsoftware.searchflicker.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.batistsoftware.searchflicker.EventBus.model.ActionEvent;
import com.batistsoftware.searchflicker.R;

import com.batistsoftware.searchflicker.EventBus.model.GetFlickerSearchPageEvent;
import com.batistsoftware.searchflicker.util.Constants;
import com.batistsoftware.searchflicker.util.FlickerRestApiClient;
import com.batistsoftware.searchflicker.util.Utils;
import com.batistsoftware.searchflicker.adapters.FlickrPhotoAdapter;

import de.greenrobot.event.EventBus;

import com.batistsoftware.searchflicker.model.FlickerPhotosPage;

/**
 * Created by JoMedia_1 on 12.02.2016.
 */
public class FlickerSearchFragment extends Fragment {

    private final String TAG = FlickerSearchFragment.class.getName();

    private GridView mGVPhotos;
    private ProgressBar mPBPhotoLoading;
    private View mRetryView;
    private FlickerPhotosPage mFlickerPhotosPage = new FlickerPhotosPage();
    private String mSearchTag;
    private ImageView mIVRetry;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_flicker, container,
                false);
        mGVPhotos = (GridView) v.findViewById(R.id.gridView);
        mGVPhotos.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                if ((scrollState == SCROLL_STATE_IDLE) && (mFlickerPhotosPage.getFlickrPhotoes() != null) && (mPBPhotoLoading.getVisibility() != View.VISIBLE)) {
                    final int count = mFlickerPhotosPage.getFlickrPhotoes().size();
                    final int lastPos = mGVPhotos.getLastVisiblePosition();
                    if (lastPos >= count - 1) {
                        if (mFlickerPhotosPage.getNextPage() != -1) {
                            sendRequestToGetData(mFlickerPhotosPage.getNextPage());
                        }
                    } else if ((mGVPhotos.getFirstVisiblePosition() == 0) && (mFlickerPhotosPage.getPage() != 0)) {
                        sendRequestToGetData(mFlickerPhotosPage.getPreviousPage());
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });
        mPBPhotoLoading = (ProgressBar) v.findViewById(R.id.pb_loading);
        mRetryView = v.findViewById(R.id.ll_retry);
        mIVRetry = (ImageView) mRetryView.findViewById(R.id.iv_retry);
        mRetryView.findViewById(R.id.btn_retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.log(Log.DEBUG, TAG, TAG + ".onClick()", null);
                sendRequestToGetData(mFlickerPhotosPage.getPage());
            }
        });
        showRetry(R.string.start_search, View.GONE, R.drawable.flickr_placeholder);
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(ActionEvent event) {
        Utils.log(Log.DEBUG, TAG, TAG + ".onEvent(ActionEvent)", null);

        switch (event.getActionId()) {
            case Constants.ACTION_ID_SEARCH:
                mSearchTag = (String) event.getExtras();
                sendRequestToGetData(0);
                break;
            case Constants.ACTION_ID_CLEAR_SEARCH:
                mFlickerPhotosPage = new FlickerPhotosPage();
                showRetry(R.string.start_search, View.GONE, R.drawable.flickr_placeholder);
                break;
        }
    }

    public void onEventMainThread(GetFlickerSearchPageEvent event) {
        Utils.log(Log.DEBUG, TAG, TAG + ".onEvent(GetFlickerSearchPageEvent)", null);
        if (event.getException() == null) {
            mFlickerPhotosPage = event.getFlickerSearchPage();
            if ((mFlickerPhotosPage.getFlickrPhotoes() != null) && (mFlickerPhotosPage.getFlickrPhotoes().size() > 0)) {
                showContent();
            } else {
                showRetry(R.string.no_content, View.GONE, R.drawable.flickr_unhappy);
            }
        } else {
            showRetry(R.string.err_get_data_verify_connection, View.GONE, R.drawable.flickr_unhappy);
        }
    }

    private void sendRequestToGetData(int page) {
        Utils.log(Log.DEBUG, TAG, TAG + ".sendRequestToGetData()", null);
        if (Utils.isNetworkAvailable(getActivity())) {
            showProgress();
            FlickerRestApiClient.getInstance().searchPhotoByTags(mSearchTag, page);
        } else {
            showRetry(R.string.offline_message, View.GONE, R.drawable.flickr_unhappy);
        }
    }

    private void showRetry(int messageResource, int buttonVisibility, int drawable) {
        Utils.log(Log.DEBUG, TAG, TAG + ".showRetry()", null);
        mGVPhotos.setVisibility(View.GONE);
        mPBPhotoLoading.setVisibility(View.GONE);
        if (drawable != -1) {
            mIVRetry.setVisibility(View.VISIBLE);
            mIVRetry.setImageDrawable(ContextCompat.getDrawable(getActivity(), drawable));
        } else {
            mIVRetry.setVisibility(View.GONE);
        }
        mRetryView.setVisibility(View.VISIBLE);
        ((TextView) mRetryView.findViewById(R.id.tv_retry_message)).setText(messageResource);
        mRetryView.findViewById(R.id.btn_retry).setVisibility(buttonVisibility);
    }

    private void showContent() {
        Utils.log(Log.DEBUG, TAG, TAG + ".showContent()", null);
        mPBPhotoLoading.setVisibility(View.GONE);
        mRetryView.setVisibility(View.GONE);
        mGVPhotos.setVisibility(View.VISIBLE);
        mGVPhotos.setAdapter(new FlickrPhotoAdapter(getActivity(), mFlickerPhotosPage.getFlickrPhotoes()));
    }

    private void showProgress() {
        mPBPhotoLoading.setVisibility(View.VISIBLE);
        mRetryView.setVisibility(View.GONE);
    }

}
