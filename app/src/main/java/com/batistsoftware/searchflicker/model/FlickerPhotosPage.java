package com.batistsoftware.searchflicker.model;

import java.util.ArrayList;

/**
 * Created by JoMedia_1 on 12.02.2016.
 */
public class FlickerPhotosPage {

    public static final String OBJECT_NAME = "photos";
    int page;
    int pages;
    int perpage;
    int total;

    ArrayList<FlickerPhoto> photo;

    public ArrayList<FlickerPhoto> getFlickrPhotoes() {
        return photo;
    }

    public int getPage(){
        return page;
    }

    public int getNextPage() {
        if (page != pages) {
            return page + 1;
        }
        return -1;
    }

    public int getPreviousPage() {
        if (page != 0) {
            return page - 1;
        }
        return -1;
    }
}
