package com.batistsoftware.searchflicker.model;

/**
 * Created by Batisto4ka on 12.02.2016.
 */
public class FlickrRestError {

    public static final String STATUS = "stat";
    public static final String MESSAGE = "message";

    public static final String STATUS_FAIL = "fail";
    String stat;
    String code;
    String message;
}
