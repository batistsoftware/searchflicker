package com.batistsoftware.searchflicker.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.batistsoftware.searchflicker.BuildConfig;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

/**
 * Created by JoMedia_1 on 01.02.2016.
 */
public class Utils {

    private static final String TAG = Utils.class.getName();

    /**
     * Use to set the image of an ImageView via Picasso.
     *
     * @param context          Context of the ImageView
     * @param imageUrl         URL where is the image file
     * @param imageView        ImageView to set the image
     * @param placeholderResId Resource ID of the drawable to display while loading the image. ignored if 0.
     * @param errorResId       Resource ID of the drawable to display if an error occurred while loading the image. ignored if 0.
     */
    public static void setImage(Context context, String imageUrl, ImageView imageView, int placeholderResId, int errorResId) {

        if (!TextUtils.isEmpty(imageUrl)) {
            try {
                RequestCreator rc = Picasso.with(context).load(imageUrl);
                if (placeholderResId > 0) {
                    rc = rc.placeholder(placeholderResId);
                } else {
                    rc = rc.noPlaceholder();
                }

                if (errorResId > 0) {
                    rc = rc.error(errorResId);
                }

                rc.into(imageView);
            } catch (Exception e) {
                log(Log.ERROR, TAG, TAG + "ERROR in setImage(). Picasso throws : " + e.getMessage(), e);
            }
        } else {
            if (placeholderResId == -1) {
                imageView.setImageResource(errorResId);
            } else {
                imageView.setImageResource(placeholderResId);
            }
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void log(int logType, String tag, String message, Exception e) {
        if (BuildConfig.DEBUG) {

            switch (logType) {
                case Log.WARN:
                    Log.w(tag, message);
                    break;
                case Log.DEBUG:
                    Log.d(tag, message);
                    break;
                case Log.ERROR:
                    Log.e(tag, message);
                    break;
                case Log.INFO:
                    Log.i(tag, message);
                    break;
                case Log.VERBOSE:
                    Log.v(tag, message);
                    break;
                default:
                    break;
            }
        }
    }


}
