package com.batistsoftware.searchflicker.util;

import android.util.Log;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.json.JSONObject;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;
import com.batistsoftware.searchflicker.EventBus.model.GetFlickerSearchPageEvent;
import de.greenrobot.event.EventBus;
import com.batistsoftware.searchflicker.model.FlickerPhotosPage;
import com.batistsoftware.searchflicker.model.FlickrRestError;

/**
 * Created by JoMedia_1 on 12.02.2016.
 */
public class FlickerRestApiClient {
    private static final String TAG = FlickerRestApiClient.class.getName();

    private static FlickerRestApiClient instance;


    private static OkHttpClient client;
    private static Gson gson;

    private final String FLICKER_REST_ROOT = "https://api.flickr.com/services/rest/";
    private final String METHOD_SEARCH = "flickr.photos.search";
    private final String FLICKER_API_KEY = "c3da85250addd5246ca19105abd27594";


    public static FlickerRestApiClient getInstance() {
        if (instance == null) {
            setupClient();
        }
        return instance;
    }

    private static void setupClient() {
        client = new OkHttpClient();
        client.setReadTimeout(10, TimeUnit.SECONDS);
        client.setConnectTimeout(10, TimeUnit.SECONDS);

        gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES) //Let the parser know that the JSON fields are lower case with underscores
                .create();

        instance = new FlickerRestApiClient();
    }

    public void searchPhotoByTags(String tags, int page) {
        Utils.log(Log.DEBUG, TAG, TAG + ".searchPhotoByTags() tags=" + tags + ", page=" + page, null);
        try {
            tags = URLEncoder.encode(tags, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Utils.log(Log.ERROR, TAG, TAG + ".searchPhotoByTags() tags=" + tags, null);
            EventBus.getDefault().post(new GetFlickerSearchPageEvent(e));
        }

        String requestURL = FLICKER_REST_ROOT;

        UrlEncodedParamBuilder paramBuilder = new UrlEncodedParamBuilder();
        paramBuilder.addParam(Constants.URL_PARAM_METHOD, METHOD_SEARCH);
        paramBuilder.addParam(Constants.URL_PARAM_API_KEY, FLICKER_API_KEY);
        paramBuilder.addParam(Constants.URL_PARAM_TAGS, tags);
        paramBuilder.addParam(Constants.URL_PARAM_FORMAT, Constants.JSON);
        paramBuilder.addParam(Constants.URL_PARAM_EXTRAS, Constants.EXTRA_SMALL_URL);
        paramBuilder.addParam(Constants.URL_PARAM_NO_JSON_CALLBACK, 1);
        paramBuilder.addParam(Constants.URL_PARAM_PER_PAGE, Constants.LIMIT);
        paramBuilder.addParam(Constants.URL_PARAM_PAGE, page);

        requestURL += "?" + paramBuilder.Build();
        Utils.log(Log.DEBUG, TAG, TAG + ".searchPhotoByTags() tags=" + tags + ", url=" + requestURL, null);
        try {
            final Request request = getRequest(requestURL);
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    Utils.log(Log.ERROR, TAG, TAG + ".searchPhotoByTags().onFailure(), url=\" + requestURL" + e, e);
                    EventBus.getDefault().post(new GetFlickerSearchPageEvent(e));
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    try {
                        String content = response.body().string();
                        JSONObject jsonObject = new JSONObject(content);
                        if ((jsonObject.has(FlickerPhotosPage.OBJECT_NAME))) {
                            FlickerPhotosPage flickerPhotosPage = gson.fromJson(jsonObject.get(FlickerPhotosPage.OBJECT_NAME).toString(), FlickerPhotosPage.class);
                            EventBus.getDefault().post(new GetFlickerSearchPageEvent(flickerPhotosPage));
                        } else if ((jsonObject.has(FlickrRestError.STATUS)) && (jsonObject.has(FlickrRestError.MESSAGE))) {
                            if (jsonObject.get(FlickrRestError.STATUS).toString().equals(FlickrRestError.STATUS_FAIL)) {
                                String errorMessage = jsonObject.get(FlickrRestError.MESSAGE).toString();
                                Utils.log(Log.ERROR, TAG, TAG + "searchPhotoByTags(), url=\" + requestURL", new Exception(errorMessage));
                                EventBus.getDefault().post(new GetFlickerSearchPageEvent(new Exception(errorMessage)));
                            }
                        } else {
                            Utils.log(Log.ERROR, TAG, TAG + "searchPhotoByTags(), url=\" + requestURL", new Exception("No such object " + FlickerPhotosPage.OBJECT_NAME));
                            EventBus.getDefault().post(new GetFlickerSearchPageEvent(new Exception("No such object " + FlickerPhotosPage.OBJECT_NAME)));
                        }
                    } catch (Exception e) {
                        Utils.log(Log.ERROR, TAG, TAG + ".searchPhotoByTags(), url=\" + requestURL" + e, e);
                        EventBus.getDefault().post(new GetFlickerSearchPageEvent(e));
                    }
                }
            });
        } catch (Exception e) {
            Utils.log(Log.ERROR, TAG, TAG + "searchPhotoByTags().onFailure(), url=\" + requestURL " + e, e);
            EventBus.getDefault().post(new GetFlickerSearchPageEvent(e));
        }
    }

    private Request getRequest(String aRequestURL) {
        Request.Builder requestBuilder = new Request.Builder()
                .url(aRequestURL);

        Request request = requestBuilder.build();
        return request;
    }

}
