package com.batistsoftware.searchflicker.util;

import java.util.LinkedList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

/**
 * Created by JoMedia_1 on 12.02.2016.
 */
public class UrlEncodedParamBuilder {

    private List<NameValuePair> mParams;

    public UrlEncodedParamBuilder() {
        mParams = new LinkedList<NameValuePair>();
    }

    public void addParam(String key, String value) {
        mParams.add(new BasicNameValuePair(key, value));
    }

    public void addParam(String key, int value) {
        mParams.add(new BasicNameValuePair(key, String.valueOf(value)));
    }

    public String Build() {
        if (mParams.size() > 0) {
            return URLEncodedUtils.format(mParams, "utf-8");
        }
        return null;
    }
}