package com.batistsoftware.searchflicker.util;

/**
 * Created by JoMedia_1 on 12.02.2016.
 */
public class Constants {
    public static final String URL_PARAM_METHOD = "method";
    public static final String URL_PARAM_API_KEY = "api_key";
    public static final String URL_PARAM_TAGS = "tags";
    public static final String URL_PARAM_FORMAT = "format";
    public static final String URL_PARAM_EXTRAS = "extras";
    public static final String URL_PARAM_NO_JSON_CALLBACK = "nojsoncallback";
    public static final String URL_PARAM_PER_PAGE = "per_page";
    public static final String URL_PARAM_PAGE = "page";

    public static final String JSON = "json";
    public static final String EXTRA_SMALL_URL = "url_s";
    public static final int LIMIT = 60;

    //Event bus action ids
    public static final int ACTION_ID_SEARCH = 101;
    public static final int ACTION_ID_CLEAR_SEARCH = 102;
}
