package com.batistsoftware.searchflicker.EventBus.model;

import com.batistsoftware.searchflicker.model.FlickerPhotosPage;

/**
 * Created by JoMedia_1 on 12.02.2016.
 */
public class GetFlickerSearchPageEvent {
    FlickerPhotosPage mFlickerPage;
    Exception mException;

    public GetFlickerSearchPageEvent(FlickerPhotosPage flickerPage) {
        mFlickerPage = flickerPage;
    }

    public GetFlickerSearchPageEvent(Exception exception) {
        mException = exception;
    }

    public FlickerPhotosPage getFlickerSearchPage() {
        return mFlickerPage;
    }

    public Exception getException() {
        return mException;
    }
}