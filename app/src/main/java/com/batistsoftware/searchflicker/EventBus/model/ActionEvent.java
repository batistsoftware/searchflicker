package com.batistsoftware.searchflicker.EventBus.model;

/**
 * Created by Batisto4ka on 13.02.2016.
 */
public class ActionEvent {
    int mActionId;
    Object mExtras;

    public ActionEvent(int actionId) {
        mActionId = actionId;
    }

    public ActionEvent(int actionId, Object extras) {
        mActionId = actionId;
        mExtras = extras;
    }

    public int getActionId() {
        return mActionId;
    }

    public Object getExtras() {
        return mExtras;
    }
}
