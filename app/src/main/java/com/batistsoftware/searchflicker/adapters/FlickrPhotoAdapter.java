package com.batistsoftware.searchflicker.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.batistsoftware.searchflicker.R;

import java.util.ArrayList;

import com.batistsoftware.searchflicker.util.Utils;
import com.batistsoftware.searchflicker.model.FlickerPhoto;

/**
 * Created by JoMedia_1 on 12.02.2016.
 */
public class FlickrPhotoAdapter extends ArrayAdapter<FlickerPhoto> {

    private class ViewHolder {
        ImageView mIVPhoto;
    }
    public FlickrPhotoAdapter(Context context, ArrayList<FlickerPhoto> items) {
        super(context, 0, items);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        FlickerPhoto flickerPhoto = getItem(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.photo_item, parent, false);
            holder.mIVPhoto = (ImageView)convertView.findViewById(R.id.iv_flickr_photo);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Utils.setImage(getContext(),flickerPhoto.getUrl(), holder.mIVPhoto, R.drawable.placeholder_image, R.drawable.flickr_unhappy);
        return convertView;
    }
}