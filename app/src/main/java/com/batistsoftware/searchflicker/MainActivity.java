package com.batistsoftware.searchflicker;

import android.app.ActionBar;
import android.app.LoaderManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


import com.batistsoftware.searchflicker.EventBus.model.ActionEvent;
import com.batistsoftware.searchflicker.contentProvider.FlickrSearchProvider;
import com.batistsoftware.searchflicker.fragments.FlickerSearchFragment;
import com.batistsoftware.searchflicker.util.Constants;
import com.batistsoftware.searchflicker.util.Utils;

import de.greenrobot.event.EventBus;


public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private final String TAG = MainActivity.class.getName();
    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getFragmentManager().beginTransaction().add(R.id.fl_container, new FlickerSearchFragment()).commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        final MenuItem mSearchMenuItem = menu.findItem(R.id.menu_item_search);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) MenuItemCompat.getActionView(mSearchMenuItem);
        mSearchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        mSearchView.setQueryHint(getText(R.string.start_search));

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!TextUtils.isEmpty(query)) {
                    SearchRecentSuggestions suggestions = new SearchRecentSuggestions(MainActivity.this,
                            FlickrSearchProvider.AUTHORITY, FlickrSearchProvider.MODE);
                    suggestions.saveRecentQuery(query, null);

                    EventBus.getDefault().post(new ActionEvent(Constants.ACTION_ID_SEARCH, query));
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String s) {
                return false;
            }
        });

        updateSearchSuggestionsAdapter();
        mSearchView.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT));
        mSearchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Utils.log(Log.DEBUG, TAG, TAG + ".onSuggestionClick() position=" + position, null);

                Cursor cursor = (Cursor) mSearchView.getSuggestionsAdapter().getItem(position);
                String query = cursor.getString(cursor.getColumnIndex(FlickrSearchProvider.QUERY_COLUMN));
                mSearchView.setQuery(query, true);
                return false;
            }
        });
        mSearchView.findViewById(R.id.search_close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.log(Log.DEBUG, TAG, TAG + ": closeSearchButton.onClick()", null);
                EditText et = (EditText) findViewById(R.id.search_src_text);
                et.setText("");
                EventBus.getDefault().post(new ActionEvent(Constants.ACTION_ID_CLEAR_SEARCH));
            }
        });

        getLoaderManager().initLoader(0, null, this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_settings) {
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(MainActivity.this,
                    FlickrSearchProvider.AUTHORITY, FlickrSearchProvider.MODE);
            suggestions.clearHistory();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateSearchSuggestionsAdapter() {
        final Cursor cursor = getContentResolver().query(FlickrSearchProvider.CONTENT_URI, null, null, null, null);
        String[] from = new String[]{FlickrSearchProvider.QUERY_COLUMN};
        int[] to = new int[]{R.id.text};
        SimpleCursorAdapter suggestionsAdapter = new SimpleCursorAdapter(this, R.layout.suggestion_list_item, cursor, from, to, 0);

        mSearchView.setSuggestionsAdapter(suggestionsAdapter);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                MainActivity.this,   // Parent activity context
                FlickrSearchProvider.CONTENT_URI,        // Table to query
                null,     // Projection to return
                null,            // No selection clause
                null,            // No selection arguments
                null);
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mSearchView.getSuggestionsAdapter().changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mSearchView.getSuggestionsAdapter().changeCursor(null);
    }
}
