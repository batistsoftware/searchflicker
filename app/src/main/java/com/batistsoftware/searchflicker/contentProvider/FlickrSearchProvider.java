package com.batistsoftware.searchflicker.contentProvider;


import android.content.SearchRecentSuggestionsProvider;
import android.net.Uri;

/**
 * Created by Batisto4ka on 12.02.2016.
 */
public class FlickrSearchProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "com.batistsoftware.searchflicker.contentProvider.FlickrSearchProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/suggestions");
    public static final String QUERY_COLUMN = "query";

    public FlickrSearchProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }

}
